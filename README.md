#demo change

# NYS SEPSIS COVID DATA COLLECTION

![Bitbucket Issues](https://img.shields.io/bitbucket/issues-raw/nys-sepsis-covid-data-collection/sepsis-covid-data-collection)
![Bitbucket Pull Requests](http://img.shields.io/bitbucket/pr-raw/nys-sepsis-covid-data-collection/sepsis-covid-data-collection)

The NYS Sepsis Covid Data Collection Project(NYSSCDC) name is a utility that allows users to do move from a manual abstraction process to an automated data pull.

## Prerequisites

<!--- TODO: add prerequisites --->

Before you begin, ensure you have met the following requirements:

## Installing NYSSCDC

<!--- TODO: add installation  --->

To install NYSSCDC, follow these steps:

Windows:

```
<installation process TBD>
```

Linux and macOS:

```
<installation process TBD>
```


## Using NYSSCDC

To use NYSSCDC, follow these steps:

```
<usage_example_TBD>
```


## Contributing to NYSSCDC
To contribute to NYSSCDC, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.

Alternatively see the documentation for [creating a pull request](https://bitbucket.org/nys-sepsis-covid-data-collection/sepsis-covid-data-collection/wiki/Contributing%20Guide).

## Contributors

Thanks to the following people who have contributed to this project:

* @collin_york
* @G. Briddick

## Contact

If you want to contact support you can reach me at <support@ipro.us>.

## License

This project uses the following license: [Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).
